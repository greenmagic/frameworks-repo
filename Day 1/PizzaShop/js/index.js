// index.js
function runTotal() {
    var total = 0;
    var small = 20;
    var medium = 50;
    var large = 90;
    var addTop = 50;
    if (document.forms[0].pizza[0].checked == true) {
        total += small;
    } else if (document.forms[0].pizza[1].checked == true) {
        total += medium;
    } else if (document.forms[0].pizza[2].checked == true) {
        total += large;
    }
    for (var i = 0; i <= 3; i++) {
        if (document.forms[0].toppings[i].checked == true) {
            total += addTop;
        }
    }
    document.f1.Total.value = total;
}